# Images and Cosign Signatures on different locations

This project shows how to store the signatures of a Docker image on different location than the image registry. 

## Generate Signing Keys

The first step is to generate the cosign keys. We chose to using the Gitlab provider with the command:

```shell
cosign generate-key-pair gitlab://<project_id>
```

For example:

Export the environment variable `GITLAB_TOKEN` with rights to create CI/CD variables on the Gitlab project that will store the cosign keys:

```shell
export GITLAB_TOKEN=glpat-Z7Dj....
```

Then generate the cosign key pair with the Gitlab provider:

```shell
$ cosign generate-key-pair gitlab://43786055
Enter password for private key: 
Enter password for private key again: 
Password written to "COSIGN_PASSWORD" variable
Private key written to "COSIGN_PRIVATE_KEY" variable
Public key written to "COSIGN_PUBLIC_KEY" variable
Public key also written to cosign.pub
```

On success, three variables are added to the project:

![variable](./img/cosign-gitlab-variables.png)

The public key can be retrieved with:

```shell
$ cosign public-key --key gitlab://43786055
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAESbuSEUBFW0yndQABomJKA3dQwbKD
cSWbGpnECsZ7IvdUj9GGGlmPpYl8H0WCHCRuWGSGX58ZiiSuUQRDQoHAxw==
-----END PUBLIC KEY-----
```

## Signing Images

Let's imagine we want to sign the image `quay.io/keycloak/keycloak:21.1.2` (image arbitrarily chosen).

Signing the artifact identified by a tag can lead to sign a different image than the intended one. It is better to identify the artifact with a digest rather than tag.

The manifest digest can be obtained by using crane:

```shell
$ crane digest quay.io/keycloak/keycloak:21.1.2
sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0
```

### Storing the signatures on an OCI registry

 The `COSIGN_REPOSITORY` environment variable can be used to specify the location of the signature repository (see https://github.com/sigstore/cosign#specifying-registry):

For example, to store the signatures on the registry `registry.gitlab.com/pseite/my-signatures`:

```shell
COSIGN_REPOSITORY=registry.gitlab.com/pseite/my-signatures
```

Authenticate to the OCI registry for signature (the signatures shall be stored as a new tags):

```
$ docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD registry.gitlab.com
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /home/paus6502/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

Storing signatures in a separate repository with the following command:

```shell
$ COSIGN_REPOSITORY=registry.gitlab.com/pseite/my-signatures cosign sign --key gitlab://43786055 quay.io/keycloak/keycloak@sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0

	The sigstore service, hosted by sigstore a Series of LF Projects, LLC, is provided pursuant to the Hosted Project Tools Terms of Use, available at https://lfprojects.org/policies/hosted-project-tools-terms-of-use/.
	Note that if your submission includes personal data associated with this signed artifact, it will be part of an immutable record.
	This may include the email address associated with the account with which you authenticate your contractual Agreement.
	This information will be used for signing this artifact and will be stored in public transparency logs and cannot be removed later, and is subject to the Immutable Record notice at https://lfprojects.org/policies/hosted-project-tools-immutable-records/.

By typing 'y', you attest that (1) you are not submitting the personal data of any other person; and (2) you understand and agree to the statement and the Agreement terms at the URLs listed above.
Are you sure you would like to continue? [y/N] y
tlog entry created with index: 37311989
Pushing signature to: pseite/my-signatures
```

We can verify that the signature is added to the appropriate OCI registry:

![](./img/disconnected-signature.png)



The storage location can also be retrieved with the command:

```shell
$ COSIGN_REPOSITORY=registry.gitlab.com/pseite/my-signatures cosign triangulate quay.io/keycloak/keycloak:21.1.2
registry.gitlab.com/pseite/my-signatures:sha256-3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0.sig
```

With the above command, the signature, and some metadata, are also uploaded to the Rekor server from Sigstrore. We can choose to not send data (aka transparency logs) to the Rekor server with the flag `--tlog-upload=false`:

```shell
$ COSIGN_REPOSITORY=registry.gitlab.com/pseite/my-signatures cosign sign --key gitlab://43786055 --tlog-upload=false quay.io/keycloak/keycloak@sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0
Pushing signature to: pseite/my-signatures
```

**Note:** we can verify on the signature registry that the signature is not duplicated, i.e. a single tag corresponds to the signature.

### Storing the signatures locally

The signature can be stored locally by using the flags `output-signature` and `output-certificate`

```shell
$ cosign sign --key gitlab://43786055 --upload=false --tlog-upload=false --output-signature=cosign-keycloak-local.sig --output-certificate=cosign-keycloak-local.crt quay.io/keycloak/keycloak@sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0
Certificate wrote in the file cosign-keycloak-local.crt
```

Two files are generated:

- `cosign-keycloak-local.sig`, which is the signature 
- `cosign-keycloak-local.crt` , which is the certificate containing the public key expected to be used for the verification signature 

```shell
$ ls cosign-keycloak-local.*
cosign-keycloak-local.crt  cosign-keycloak-local.sig
```

## Verifying Signatures

### Signatures stored on an OCI registry

A signature is verifified with the command `cosign verify`. The environment variable `COSIGN_REPOSITORY` is used to indicate where are stored the signatures:

```shell
$ COSIGN_REPOSITORY=registry.gitlab.com/pseite/my-signatures cosign verify --key gitlab://43786055 quay.io/keycloak/keycloak@sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0

Verification for quay.io/keycloak/keycloak@sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0 --
The following checks were performed on each of these signatures:

  - The cosign claims were validated
  - Existence of the claims in the transparency log was verified offline
  - The signatures were verified against the specified public key

[{"critical":{"identity":{"docker-reference":"quay.io/keycloak/keycloak"},"image":{"docker-manifest-digest":"sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0"},"type":"cosign container image signature"},"optional":{"Bundle":{"SignedEntryTimestamp":"MEUCIFORDDatHMC0i4iHYTdQAcIvwBNuv9WaLiU9p9c/WhDLAiEA6D3BBPx9l3I3Afz2wo/6lizGRVn8Hdv15aHggoSrk2o=","Payload":{"body":"eyJhcGlWZXJzaW9uIjoiMC4wLjEiLCJraW5kIjoiaGFzaGVkcmVrb3JkIiwic3BlYyI6eyJkYXRhIjp7Imhhc2giOnsiYWxnb3JpdGhtIjoic2....RXRGV1MwdExTMHRDZz09In19fX0=","integratedTime":1695133330,"logIndex":37311989,"logID":"c0d23d6ad406973f9559f3ba2d1ca01f84147d8ffc5b8445c224f98b9591801d"}}}}]
```

If the image is not tampered, the verification of the tag (instead of the digest) shall succeed. 

```shell
$ COSIGN_REPOSITORY=registry.gitlab.com/pseite/my-signatures cosign verify --key gitlab://43786055 quay.io/keycloak/keycloak:21.1.2                                                                 

Verification for quay.io/keycloak/keycloak:21.1.2 --
The following checks were performed on each of these signatures:

  - The cosign claims were validated
  - Existence of the claims in the transparency log was verified offline
  - The signatures were verified against the specified public key

[{"critical":{"identity":{"docker-reference":"quay.io/keycloak/keycloak"},"image":{"docker-manifest-digest":"sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0"},"type":"cosign container image signature"},"optional":{"Bundle":{"SignedEntryTimestamp":"MEUCIFORDDatHMC0i4iHYTdQAcIvwBNuv9WaLiU9p9c/WhDLAiEA6D3BBPx9l3I3Afz2wo/6lizGRVn8Hdv15aHggoSrk2o=","Payload":{"body":"eyJhcGlWZXJzaW9uIjoiMC4wLjEiLCJraW5kIjoiaGFzaGVkcmVrb3JkIiwic3BlYyI6eyJkYXRhIjp7Imhhc2giOnsiYWxnb3JpdGhtIjoic2hhMjU2IiwidmFsdWUiOiIzMzQzZDM1MzBmNWU4ZWUyNWI3NGM2MzY0OTM2ODI2OWJkOTQxNWI2NWU5ZTc3MGIyZGI1OTY4NTQzN...FIxTkhXRFU0V21scFUzVlZVVkpFVVc5SVFYaDNQVDBLTFMwdExTMUZUa1FnVUZWQ1RFbERJRXRGV1MwdExTMHRDZz09In19fX0=","integratedTime":1695133330,"logIndex":37311989,"logID":"c0d23d6ad406973f9559f3ba2d1ca01f84147d8ffc5b8445c224f98b9591801d"}}}}]
```

If transparency logs have been ignored during signing, we must use the option `insecure-ignore-tlog` to not try to download metadata that do not exist: 

```shell
$ COSIGN_REPOSITORY=registry.gitlab.com/pseite/my-signatures cosign verify --key gitlab://43786055 quay.io/keycloak/keycloak:21.1.2 --insecure-ignore-tlog | jq
WARNING: Skipping tlog verification is an insecure practice that lacks of transparency and auditability verification for the signature.

Verification for quay.io/keycloak/keycloak:21.1.2 --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key
[
  {
    "critical": {
      "identity": {
        "docker-reference": "quay.io/keycloak/keycloak"
      },
      "image": {
        "docker-manifest-digest": "sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0"
      },
      "type": "cosign container image signature"
    },
    "optional": {
      "Bundle": {
        "SignedEntryTimestamp": "MEUCIFORDDatHMC0i4iHYTdQAcIvwBNuv9WaLiU9p9c/WhDLAiEA6D3BBPx9l3I3Afz2wo/6lizGRVn8Hdv15aHggoSrk2o=",
        "Payload": {
          "body": "eyJhcGlWZXJzaW9uIjoiMC4wLjEiLCJraW5kIjoiaGFzaGVkcmVrb3JkIiwic3BlYyI6eyJkYXRhIjp7Imhhc2giOnsiYWxnb3JpdGhtIjoic2hhMjU2IiwidmFsdWUiOiIzMzQzZDM1MzBmNWU4ZWUyNWI3NGM2MzY0OTM2ODI2OWJkOTQxNWI2NWU5ZTc3MGIyZGI1OTY4NTQzNTY3NGU5In19LCJzaWduYXR1cmUiOnsiY29udGVudCI6Ik1FWUNJUUR1V2pNU0ZOY3hBVWpsUTJJTERzbG00ZUlFRUZhOFU4cDNrbTQ4Si9ZWWV3SWhBTzV4Tnk4aXFRUEtmNnY2blA3R1p2STU3cGlTbVhUOSs4ZWN0V1ZsTDVyOSIsInB1YmxpY0tleSI6eyJjb250ZW50IjoiTFMwdExTMUNSVWRKVGlCUVZVSk1TVU1nUzBWWkxTMHRMUzBLVFVacmQwVjNXVWhMYjFwSmVtb3dRMEZSV1VsTGIxcEplbW93UkVGUlkwUlJaMEZGVTJKMVUwVlZRa1pYTUhsdVpGRkJRbTl0U2t0Qk0yUlJkMkpMUkFwalUxZGlSM0J1UlVOeldqZEpkbVJWYWpsSFIwZHNiVkJ3V1d3NFNEQlhRMGhEVW5WWFIxTkhXRFU0V21scFUzVlZVVkpFVVc5SVFYaDNQVDBLTFMwdExTMUZUa1FnVUZWQ1RFbERJRXRGV1MwdExTMHRDZz09In19fX0=",
          "integratedTime": 1695133330,
          "logIndex": 37311989,
          "logID": "c0d23d6ad406973f9559f3ba2d1ca01f84147d8ffc5b8445c224f98b9591801d"
        }
      }
    }
  }
]
```

Below is the payload decoded for the sake of illustration:

```json
{
  "apiVersion": "0.0.1",
  "kind": "hashedrekord",
  "spec": {
    "data": {
      "hash": {
        "algorithm": "sha256",
        "value": "3343d3530f5e8ee25b74c63649368269bd9415b65e9e770b2db59685435674e9"
      }
    },
    "signature": {
      "content": "MEYCIQDuWjMSFNcxAUjlQ2ILDslm4eIEEFa8U8p3km48J/YYewIhAO5xNy8iqQPKf6v6nP7GZvI57piSmXT9+8ectWVlL5r9",
      "publicKey": {
        "content": "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFU2J1U0VVQkZXMHluZFFBQm9tSktBM2RRd2JLRApjU1diR3BuRUNzWjdJdmRVajlHR0dsbVBwWWw4SDBXQ0hDUnVXR1NHWDU4WmlpU3VVUVJEUW9IQXh3PT0KLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg=="
      }
    }
  }
}
```



### Signatures stored locally

```shell
$ $ cosign verify --key gitlab://43786055 --insecure-ignore-tlog   --signature=cosign-keycloak-local.sig --key=cosign-keycloak-local.crt quay.io/keycloak/keycloak:21.1.2
WARNING: Skipping tlog verification is an insecure practice that lacks of transparency and auditability verification for the signature.
WARNING: using obsolete implied signature payload data (with digested reference quay.io/keycloak/keycloak@sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0); specify it explicitly with --payload instead

Verification for quay.io/keycloak/keycloak:21.1.2 --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key

[{"critical":{"identity":{"docker-reference":"quay.io/keycloak/keycloak"},"image":{"docker-manifest-digest":"sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0"},"type":"cosign container image signature"},"optional":null}]
```

Note the payload that only contains the image digest.

### Kyverno Policy to Verifying Image Signatures using a signature repository 

Quoting https://kyverno.io/docs/writing-policies/verify-images/sigstore/#using-a-signature-repository: in the Kyverno policy rule, specify the repository for each image.

In our example, the kyverno policy should look like:

```yaml
...
verifyImages:
- imageReferences:
  - quay.io/keycloak/keycloak*
  repository: "registry.gitlab.com/pseite/my-signatures"
  attestors:
  - entries:
    - keys:
        publicKeys: |-
           -----BEGIN PUBLIC KEY-----
           MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAESbuSEUBFW0yndQABomJKA3dQwbKD
           cSWbGpnECsZ7IvdUj9GGGlmPpYl8H0WCHCRuWGSGX58ZiiSuUQRDQoHAxw==
           -----END PUBLIC KEY-----         
...
```

## References

- https://github.com/sigstore/cosign